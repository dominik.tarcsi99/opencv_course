#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;


/////////// Image ///////////
int main(int argv, char** argc)
{
	Mat image = imread("Resources/flower.jpg", IMREAD_GRAYSCALE);
	// Initialize variables for storing intermediate results
	Mat imgBlur, imgCanny, imgDil, imgErode;

	// Apply Gaussian Blur on the input image
	GaussianBlur(image, imgBlur, Size(5, 5), 0);

	// Apply Canny Edge Detector on the blurred image
	Canny(imgBlur, imgCanny, 20, 60);

	// Display the intermediate results
	imshow("Blur", imgBlur);
	imshow("Canny", imgCanny);

	// Wait until a key is pressed
	waitKey(0);

	return 0;

}