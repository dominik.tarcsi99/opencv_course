#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <iostream>

// Import the OpenCV and C++ standard libraries
using namespace cv;
using namespace std;

int main() {

	// Define a matrix to store the image frames from the webcam
	Mat img;

	// Open the default webcam
	VideoCapture cap(0);

	// Load the Haar cascade classifier for Russian license plates
	CascadeClassifier plateCascade;
	plateCascade.load("Resources/haarcascade_russian_plate_number.xml");

	// If the classifier XML file is not loaded, print an error message
	if (plateCascade.empty()) {
		cout << "ERROR: XML file not loaded" << endl;
		return -1;
	}

	// Create a vector to store the detected license plate rectangles
	vector<Rect> plates;

	// Start the webcam loop
	while (true) {

		// Read a new frame from the webcam
		cap.read(img);

		// Detect license plates in the current frame
		plateCascade.detectMultiScale(img, plates, 1.1, 10);

		// Loop through all detected license plates
		for (int i = 0; i < plates.size(); i++) {

			// Crop the license plate from the current frame
			Mat imgCrop = img(plates[i]);

			// Write the cropped license plate to a file
			imwrite("Resources/Plates/" + to_string(i) + ".png", imgCrop);

			// Draw a rectangle around the license plate
			rectangle(img, plates[i].tl(), plates[i].br(), Scalar(255, 0, 255), 3);
		}

		// Display the processed frame
		imshow("Image", img);

		// Wait for 1ms to update the frame
		waitKey(1);
	}

	// Release the webcam resources
	cap.release();

	return 0;
}
