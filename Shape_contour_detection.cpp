#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

// Function to get contours from a binary image and draw them on the original image
void getContours(Mat& imgDil, Mat& img) {
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(imgDil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	// Vector to store the bounding rectangles of the contours
	vector<Rect> boundRect(contours.size());

	// Vector to store the polygonal approximations of the contours
	vector<vector<Point> > conPoly(contours.size());

	// Loop through all the contours
	for (int i = 0; i < contours.size(); i++) {
		// Approximate the contour with a polygon
		approxPolyDP(Mat(contours[i]), conPoly[i], 3, true);

		// Get the bounding rectangle of the polygon
		boundRect[i] = boundingRect(Mat(conPoly[i]));

		// Determine the type of object based on the number of sides of the polygon
		string objectType;
		if (conPoly[i].size() == 3) {
			objectType = "Triangle";
		}
		else if (conPoly[i].size() == 4) {
			objectType = "Rectangle";
		}
		else if (conPoly[i].size() == 5) {
			objectType = "Pentagon";
		}
		else {
			objectType = "Circle";
		}

		// Draw the contour and the bounding rectangle on the original image
		drawContours(img, conPoly, i, Scalar(255, 0, 255), 2);
		rectangle(img, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 5);
		putText(img, objectType, { boundRect[i].x,boundRect[i].y - 5 }, FONT_HERSHEY_PLAIN, 1, Scalar(0, 69, 255), 2);
	}
}


void main() {
	// Define the path to the image file
	string path = "Resources/shapes.png";

	// Read the image into a matrix
	Mat img = imread(path);

	// Create matrices to store intermediate results
	Mat imgGray, imgBlur, imgCanny, imgDil, imgErode;

	// Preprocessing
	// Convert the image to grayscale
	cvtColor(img, imgGray, COLOR_BGR2GRAY);

	// Apply Gaussian Blur to smooth out the image
	GaussianBlur(imgGray, imgBlur, Size(3, 3), 3, 0);

	// Apply Canny edge detection to the blurred image
	Canny(imgBlur, imgCanny, 25, 75);

	// Apply dilation to the Canny image to thicken the edges
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(imgCanny, imgDil, kernel);

	// Get the contours from the dilated image
	getContours(imgDil, img);

	// Show the final processed image
	imshow("Image", img);

	// Show intermediate images (uncomment to see)
	//imshow("Image Gray", imgGray);
	//imshow("Image Blur", imgBlur);
	//imshow("Image Canny", imgCanny);
	//imshow("Image Dil", imgDil);

	// Wait for a key press to end the program
	waitKey(0);
}
